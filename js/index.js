var appIndex=angular.module('index',['ngFileUpload']);

appIndex.controller('indexController',function($scope,$window){
    $scope.orderby="fecha";
    $scope.select={};
    $scope.comprobanteSelect={};
    $scope.cuentas=[
        {codigo:1,nombre:'activo'},
        {codigo:2,nombre:'activo'},
        {codigo:3,nombre:'activo'},
        {codigo:4,nombre:'activo'},
        {codigo:5,nombre:'activo'},
        {codigo:6,nombre:'activo'},
        {codigo:7,nombre:'activo'},
        {codigo:8,nombre:'activo'},
        {codigo:9,nombre:'activo'},
    ]


    ///check 
    $scope.selected=[];
    $scope.checkAll=function(){
        if ($scope.selectAll){
            angular.forEach($scope.Customers,(item) => {
                idx=$scope.selected.indexOf(item);
                if (idx>=0){
                    return true;
                }else{
                    $scope.selected.push(item);
                }
            });
        }else{
            $scope.selected=[];
        }
    }
    $scope.exist=function(item){
        return $scope.selected.indexOf(item) > -1;
    }

    $scope.toggleSelection=function(item){
        var idx=$scope.selected.indexOf(item)
        if(idx>-1){
            $scope.selected.splice(idx,1);
        }else{
            $scope.selected.push(item);
        }
    }


    $scope.SelectFile = function (file) {
        $scope.SelectedFile = file;
    };

    $scope.tipo=['banco','caja']


    $scope.ver=function(item){
        // alert(JSON.stringify(item));
        $scope.select=item;
    }

    $scope.verComprobante=function(item){
        // alert(JSON.stringify(item));
        $scope.comprobanteSelect=item;
    }

    $scope.limpiar=function(){
        $scope.Customers=[];
    }

    $scope.crearExcel=function(){
        console.log("escribir");
        var data = [
            {"name":"John", "city": "Seattle"},
            {"name":"Mike", "city": "Los Angeles"},
            {"name":"Zach", "city": "New York"}
        ];
        
        /* this line is only needed if you are not adding a script tag reference */
        if(typeof XLSX == 'undefined') XLSX = require('xlsx');
        
        /* make the worksheet */
        var ws = XLSX.utils.json_to_sheet(data);
        
        /* add to workbook */
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "People");
        
        /* generate an XLSX file */
        XLSX.writeFile(wb, "sheetjs.xlsx");
    }

    $scope.Upload = function () {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test($scope.SelectedFile.name.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        $scope.ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString($scope.SelectedFile);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        $scope.ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer($scope.SelectedFile);
                }
            } else {
                $window.alert("This browser does not support HTML5.");
            }
        } else {
            $window.alert("Please upload a valid Excel file.");
        }
    };

    $scope.ProcessExcel = function (data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_json(workbook.Sheets[firstSheet],{defval:""});
    
        //Display the data from Excel file in Table.
        $scope.$apply(function () {
            $scope.Customers = excelRows;
            $scope.IsVisible = true;
            // $scope.Customers.forEach(element => {
            //     console.log(Object.keys(element));    
            // });
            console.log($scope.Customers);                    
            
            // var auz=[];
            // var copiedObj=$scope.Customers[0];                                  
            // $scope.Customers.forEach((copiedObj)=>{                    
            //     var alfa=65 ;
            //     var changed={};
            //     Object.keys(copiedObj).forEach(key => {
            //         if(key.includes("FECHA")){
            //             var arrfecha=copiedObj[key].split("/");
                        
            //             var ndate=new Date(1,1,1);
            //             ndate.setDate(parseInt(arrfecha[0]));
            //             ndate.setMonth(parseInt(arrfecha[1])-1);
            //             ndate.setFullYear(parseInt("20"+arrfecha[2]));
            //             changed[String.fromCharCode(alfa)] =ndate;
            //             changed[String.fromCharCode(75)] =copiedObj[key];
            //             // changed[String.fromCharCode(alfa)] = new Date(copiedObj[key]);
                        
                        
            //         }else{
            //             changed[String.fromCharCode(alfa)] = copiedObj[key];
            //         }
            //         alfa++;  
            //     });   
            //     auz.push(changed);
            // });
            // $scope.Customers=auz;
            console.log($scope.Customers);
            
        });
    };
});